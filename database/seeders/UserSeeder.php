<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('slug', 'moderator')->first();
        $moderator = Role::where('slug', 'admin')->first();
        $creator = Role::where('slug', 'creator')->first();
        $manageArticle = Permission::where('slug', 'manage-articles')->first();
        $publishArticle = Permission::where('slug', 'published-article')->first();
        $createArticle = Permission::where('slug', 'create-article')->first();
        $user1 = new User();
        $user1->name = 'Jhun Daewoo';
        $user1->email = 'jhon@deo.com';
        $user1->password = bcrypt('secret');
        $user1->save();
        $user1->roles()->attach($admin);
        $user1->permissions()->attach($manageArticle);

        $user2 = new User();
        $user2->name = 'Mike Thomas';
        $user2->email = 'mike@thomas.com';
        $user2->password = bcrypt('secret');
        $user2->save();
        $user2->roles()->attach($moderator);
        $user2->permissions()->attach($publishArticle);

        $user3 = new User();
        $user3->name = 'Young Forever';
        $user3->email = 'you@dforever.com';
        $user3->password = bcrypt('secret');
        $user3->save();
        $user3->roles()->attach($creator);
        $user3->permissions()->attach($createArticle);
    }
}
