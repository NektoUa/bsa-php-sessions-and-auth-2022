<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manageUser = new Permission();
        $manageUser->name = 'Manage articles';
        $manageUser->slug = 'manage-articles';
        $manageUser->save();
        $createTasks = new Permission();
        $createTasks->name = 'Published Article';
        $createTasks->slug = 'published-article';
        $createTasks->save();
        $createTasks = new Permission();
        $createTasks->name = 'Create Article';
        $createTasks->slug = 'create-article';
        $createTasks->save();
    }
}
