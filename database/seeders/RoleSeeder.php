<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manager = new Role();
        $manager->name = 'Admin';
        $manager->slug = 'admin';
        $manager->save();
        $developer = new Role();
        $developer->name = 'Moderator';
        $developer->slug = 'moderator';
        $developer->save();
        $developer = new Role();
        $developer->name = 'Creator';
        $developer->slug = 'creator';
        $developer->save();
    }
}
